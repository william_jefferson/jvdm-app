import { Component, OnInit } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { PrayerGroupService } from 'src/app/services/prayer-group.services';
import { RolesService } from 'src/app/services/roles.services';
import { UsersService } from 'src/app/services/users.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.page.html',
  styleUrls: ['./my-profile.page.scss'],
})
export class MyProfilePage implements OnInit {
  userInfo: any = {};
  prayerGroups: any[] = [];
  constructor(
    public authService: AuthenticationService,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public usersService: UsersService,
    public router: Router,
    public rolesService: RolesService,
    public prayerGroupService: PrayerGroupService,) {
    this.authService.userInfo.subscribe((res) => {
      this.userInfo = res;
      console.log(res);

    });
  }

  ngOnInit() {
    this.getPrayerGroups();
  } async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Are you sure? Do you want to delete your account',
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'No',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'Yes',
          cssClass: 'alert-button-confirm',
          handler: (() => {
            this.deleteMyAccount();
          })
        },
      ],
    });

    await alert.present();
  }

  async getPrayerGroups() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.prayerGroupService.getPrayerGroups().subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.prayerGroups = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }
  async deleteMyAccount() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.authService.deleteAccount(this.userInfo.id).subscribe(async (res) => {
      console.log(res);
      if (res.code === 200) {
        this.router.navigateByUrl('/task', { replaceUrl: true });
        this.authService.logout();
      }
      await loading.dismiss();
    }, async (res) => {
      console.log(res);
      await loading.dismiss();

    });
  }

}
