import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BrshibuealayilPageRoutingModule } from './brshibuealayil-routing.module';

import { BrshibuealayilPage } from './brshibuealayil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BrshibuealayilPageRoutingModule
  ],
  declarations: [BrshibuealayilPage]
})
export class BrshibuealayilPageModule { }
