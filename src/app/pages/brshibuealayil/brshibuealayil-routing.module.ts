import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BrshibuealayilPage } from './brshibuealayil.page';

const routes: Routes = [
  {
    path: '',
    component: BrshibuealayilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BrshibuealayilPageRoutingModule {}
