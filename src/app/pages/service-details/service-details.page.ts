import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ScheduleStatus } from 'src/app/services/Constants';
import { CounsellingService } from 'src/app/services/counselling.services';
import { CountryService } from 'src/app/services/country.services';
import { PrayerGroupService } from 'src/app/services/prayer-group.services';
import { RolesService } from 'src/app/services/roles.services';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.page.html',
  styleUrls: ['./service-details.page.scss'],
})
export class ServiceDetailsPage implements OnInit {
  heading = 'Service';
  mainImg = '';
  userInfo: any = {};
  prayerGroups: any[];
  prayerGroup: any;
  countries: any;
  myCountry: any;
  phonenumber: any;
  usernotes = '';
  errorMessage = '';
  constructor(
    route: ActivatedRoute,
    public authService: AuthenticationService,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public usersService: UsersService,
    public rolesService: RolesService,
    public countriesService: CountryService,
    public counsellingService: CounsellingService,
    public prayerGroupService: PrayerGroupService) {
    const servicename: Observable<string> = route.params.pipe(map(p => p.servicename));
    servicename.subscribe((name) => {
      if (name === 'councling') {
        this.heading = 'Councling Service';
        this.mainImg = '../../../assets/images/img8.jpg';
      }
    });
    this.authService.userInfo.subscribe((res) => {
      this.userInfo = res;
      this.phonenumber = this.userInfo.phone;
      console.log(res);
    });
  }
  ngOnInit() {
    this.getPrayerGroups();
    this.getCountries();
  }
  countryChange(ev) {
    if (!this.phonenumber) { this.phonenumber = this.myCountry.phoneExt; }
  }
  async updateUserInfo() {
    const loading = await this.loadingController.create();
    await loading.present();
    const userData = {
      phone: this.phonenumber,
      countryId: this.myCountry.id
    };
    this.usersService.updateUserInfo(this.userInfo.id, userData).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          console.log(res);
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }
  async getPrayerGroups() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.prayerGroupService.getPrayerGroups().subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.prayerGroups = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }
  async getCountries() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.countriesService.getCountries().subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.countries = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }
  prayerGrpChange(ev) {
    console.log(this.prayerGroup);
    //this.rmPrayerGroup(this.prayerGroup.id, 6, this.userInfo.id)
    this.addPrayerGroup(this.prayerGroup.id, 6, this.userInfo.id);
  }
  rmPrayerGroup(prayerGrpid, roleId, user) {
    const rm_user = [{ id: user }];
    const add_user = [];
    const req = {
      assignUsers: add_user,
      removeUsers: rm_user,
      roleId,
      prayerGroupId: prayerGrpid
    };
    this.prayerGroupService.prayergroupsAssignMembers(req).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }
  addPrayerGroup(prayerGrpid, roleId, user) {
    const rmUser = [];
    const addUser = [{ id: user }];
    const req = {
      assignUsers: addUser,
      removeUsers: rmUser,
      roleId,
      prayerGroupId: prayerGrpid
    };
    this.prayerGroupService.prayergroupsAssignMembers(req).subscribe((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  async createSchedulePre() {
    this.updateUserInfo();
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    const register = {
      userId: this.userInfo.id,
      prayerGroupId: this.prayerGroup.id,
      note: this.usernotes,
      status: ScheduleStatus.pending
    };
    console.log(register);
    this.counsellingService.createCounselling(register).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          await loading.dismiss();
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Success',
            message: 'Counceling Requested successfully. Our team member will contact you soon',
            buttons: ['OK']
          });
          await alert.present();
          const { role } = await alert.onDidDismiss();
          console.log('onDidDismiss resolved with role', role);
        }
      },
      async (res) => {
        console.log(res);

        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
}
