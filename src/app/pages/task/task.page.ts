import { Component, OnInit } from '@angular/core';
import { SchedulePage } from '../schedule/schedule.page';
import { MenuController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/services/notification.services';
import { PrayersService } from 'src/app/services/prayers.services';
import { AuthenticationService } from 'src/app/services/authentication.service';

export interface Task {
  title: string;
  icon: string;
  count_now: number;
  count_started: number;
  color: string;
}

export interface Project {
  title: string;
  subTitle: string;
  colorHex: string;
  color: string;
  percentage: number;
}


@Component({
  selector: 'app-task',
  templateUrl: './task.page.html',
  styleUrls: ['./task.page.scss'],
})
export class TaskPage implements OnInit {

  prayers: any[] = [];

  projects: Project[] = [
    {
      title: 'Medical App', subTitle: '9 hours progress', percentage: 25, color: 'success', colorHex: '#309397'
    },
    {
      title: 'Sport App', subTitle: '40 hours progress', percentage: 75, color: 'danger', colorHex: '#e46472'
    },
    {
      title: 'Farming App', subTitle: '2 hours progress', percentage: 5, color: 'primary', colorHex: '#F9BE7C'
    },
    {
      title: 'Physiotherapy App', subTitle: '56 hours progress', percentage: 95, color: 'blue', colorHex: '#6488e4'
    }
  ];

  n = 10;
  constructor(
    private modalController: ModalController,
    public menuCtrl: MenuController,
    public notificationService: NotificationService,
    public router: Router,
    public prayersService: PrayersService,
    public authService: AuthenticationService,
  ) {
    this.menuCtrl.enable(true);
  }

  ngOnInit() {
    this.getPrayers();
  }


  async openSchebdule() {
    const modal = await this.modalController.create({
      component: SchedulePage
    });
    return await modal.present();
  }
  getPrayers() {
    this.prayersService.getWeeklyPrayers().subscribe((res) => {
      console.log(res);
      if (res.code === 200) {
        this.prayers = res.data;
      }
    }, (err) => {

    });
  }
  doRefresh(event) {
    this.getPrayers();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  gotoService(servicename) {
    console.log(this.authService.isAuthenticated.value);
    if (this.authService.isAuthenticated.value) {
      this.router.navigateByUrl('/service-details/' + servicename, { replaceUrl: false });
    } else {
      this.router.navigateByUrl('/login', { replaceUrl: true });
    }
  }

}
