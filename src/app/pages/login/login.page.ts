/* eslint-disable @typescript-eslint/naming-convention */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  errorMessage: string = '';
  credentials: FormGroup;
  constructor(
    private fb: FormBuilder,
    public menuCtrl: MenuController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public authService: AuthenticationService,
    public router: Router,
  ) { 
    this.menuCtrl.enable(false);
  }
  get email() {
    return this.credentials.get('email');
  }
  get password() {
    return this.credentials.get('password');
  }
  ngOnInit() {
    this.credentials = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]], //Enaba@305040
    });
  }
  async login() {

    const loading = await this.loadingController.create();
    await loading.present();
    const login = {
      "email": this.credentials.get('email')?.value,
      "password": this.credentials.get('password')?.value,
    }
    this.errorMessage = '';
    this.authService.login(login).subscribe(
      async (res) => {
        console.log(res);
        await loading.dismiss();
        this.router.navigateByUrl('/task', { replaceUrl: true });
        this.authService.isAuthenticated.next(true);
      },
      async (res) => {
        console.log(res);
        if(res.error.code === 400){
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
}