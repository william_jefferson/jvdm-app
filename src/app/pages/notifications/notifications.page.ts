import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Notification } from 'src/app/services/Constants';
import { NotificationService } from 'src/app/services/notification.services';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  notifications : any[] = [];
  notificationType = Notification;
  constructor(
    public notificationService: NotificationService,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public authService: AuthenticationService,
    public router: Router,
  ) {
    this.notificationService.newnotification.subscribe((res)=>{
      this.getNotifications();
    });
   }

  ngOnInit() {
    this.getNotifications();
  }

  async getNotifications(){
  
    const loading = await this.loadingController.create();
    await loading.present();

    this.notificationService.getNotifications().subscribe(
      async (res) => {
        console.log(res);
        if(res.code === 200){
          this.notifications = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  
  }
  doRefresh(event){
    this.getNotifications();
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }
  openurl(notification){
    console.log(notification);
    if(notification.type === Notification.LINK || notification.type === Notification.PRAYER  || notification.type === Notification.YOUTUBE){
      window.open(notification.url,'_blank');
    }
  }
}
