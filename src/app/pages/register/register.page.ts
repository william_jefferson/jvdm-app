import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  credentials: FormGroup;
  errorMessage = '';
  constructor(
    private fb: FormBuilder,
    public menuCtrl: MenuController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public authService: AuthenticationService,
    public router: Router
  ) {
    this.menuCtrl.enable(false);
   }

  ngOnInit() {
    this.credentials = this.fb.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }
  async register() {

    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    const register = {
      fullName: this.credentials.get('fullName')?.value,
      email: this.credentials.get('email')?.value,
      username: this.credentials.get('email')?.value,
      password: this.credentials.get('password')?.value,
      userRole: [{
        roleId: 1
      }]
    };
    console.log(register);
    this.authService.register(register).subscribe(
      async (res) => {
        console.log(res);
        await loading.dismiss();
        this.router.navigateByUrl('/task', { replaceUrl: true });
        this.authService.isAuthenticated.next(true);
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
  get fullName() {
    return this.credentials.get('fullName');
  }
  get email() {
    return this.credentials.get('email');
  }
  get password() {
    return this.credentials.get('password');
  }
}
