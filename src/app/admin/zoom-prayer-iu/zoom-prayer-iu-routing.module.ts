import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZoomPrayerIuPage } from './zoom-prayer-iu.page';

const routes: Routes = [
  {
    path: '',
    component: ZoomPrayerIuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoomPrayerIuPageRoutingModule {}
