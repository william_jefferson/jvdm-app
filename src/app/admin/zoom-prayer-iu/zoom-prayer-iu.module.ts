import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoomPrayerIuPageRoutingModule } from './zoom-prayer-iu-routing.module';

import { ZoomPrayerIuPage } from './zoom-prayer-iu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ZoomPrayerIuPageRoutingModule
  ],
  declarations: [ZoomPrayerIuPage]
})
export class ZoomPrayerIuPageModule {}
