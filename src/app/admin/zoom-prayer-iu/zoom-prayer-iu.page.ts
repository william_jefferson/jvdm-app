import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-zoom-prayer-iu',
  templateUrl: './zoom-prayer-iu.page.html',
  styleUrls: ['./zoom-prayer-iu.page.scss'],
})
export class ZoomPrayerIuPage implements OnInit {

  errorMessage: string = '';
  credentials: FormGroup;
  constructor(
    private fb: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public authService: AuthenticationService,
    public router: Router,
  ) { }
 
  ngOnInit() {
    this.credentials = this.fb.group({
      title: ['', [Validators.required]],
      time:['', [Validators.required]],
      zoomId: ['', [Validators.required]],
      zoomLink: ['', [Validators.required]],
    });
  }
  async addNewZoomPrayer() {

    const loading = await this.loadingController.create();
    await loading.present();
 
    console.log(this.credentials.get('time')?.value);
    
    const login = {
      "": this.credentials.get('email')?.value,
      "password": this.credentials.get('password')?.value,
    }
    this.errorMessage = '';
    // this.authService.login(login).subscribe(
    //   async (res) => {
    //     console.log(res);
    //     await loading.dismiss();
    //     this.router.navigateByUrl('/home', { replaceUrl: true });
    //     this.authService.isAuthenticated.next(true);
    //   },
    //   async (res) => {
    //     console.log(res);
    //     if(res.error.code === 400){
    //       this.errorMessage = res.error.message;
    //     }
    //     await loading.dismiss();
    //   }
    // );
  }
  get title() {
    return this.credentials.get('title');
  }
  get time() {
    return this.credentials.get('time');
  }
  get zoomId() {
    return this.credentials.get('zoomId');
  }
  get zoomLink() {
    return this.credentials.get('zoomLink');
  }
}
