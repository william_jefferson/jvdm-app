import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CampMeetingIuPageRoutingModule } from './camp-meeting-iu-routing.module';

import { CampMeetingIuPage } from './camp-meeting-iu.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CampMeetingIuPageRoutingModule
  ],
  declarations: [CampMeetingIuPage]
})
export class CampMeetingIuPageModule {}
