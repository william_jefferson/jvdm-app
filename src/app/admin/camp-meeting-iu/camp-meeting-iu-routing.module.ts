import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampMeetingIuPage } from './camp-meeting-iu.page';

const routes: Routes = [
  {
    path: '',
    component: CampMeetingIuPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampMeetingIuPageRoutingModule {}
