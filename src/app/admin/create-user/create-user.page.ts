import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { UsersService } from 'src/app/services/users.services';
import { RolesService } from 'src/app/services/roles.services';
import { CounclingHistoryComponent } from 'src/app/modal/councling-history/councling-history.component';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.page.html',
  styleUrls: ['./create-user.page.scss'],
})
export class CreateUserPage implements OnInit {

  credentials: FormGroup;
  errorMessage = '';
  roles: any[] = [];
  constructor(
    private fb: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public usersService: UsersService,
    public rolesService: RolesService,
    public router: Router
  ) { }
  get fullName() {
    return this.credentials.get('fullName');
  }
  get email() {
    return this.credentials.get('email');
  }
  get roleId() {
    return this.credentials.get('roleId');
  }
  get password() {
    return this.credentials.get('password');
  }
  ngOnInit() {
    this.getRoles();
    this.credentials = this.fb.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      roleId: [, [Validators.required]],
    });
  }

  async register() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    const register = {
      fullName: this.credentials.get('fullName')?.value,
      email: this.credentials.get('email')?.value,
      username: this.credentials.get('email')?.value,
      password: this.credentials.get('password')?.value,
      userRole:[{
        roleId:this.credentials.get('roleId')?.value
    }]
    };
    console.log(register);
     this.usersService.createUser(register).subscribe(
      async (res) => {
        console.log(res);
        await loading.dismiss();
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Success',
          message: 'User Created Successfully',
          buttons: ['OK']
        });
        await alert.present();
        this.credentials.reset();
        const { role } = await alert.onDidDismiss();
        console.log('onDidDismiss resolved with role', role);
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
  async getRoles() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.rolesService.getRole().subscribe(
      async (res) => {
        console.log(res);
         this.roles = res.data;
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }


}
