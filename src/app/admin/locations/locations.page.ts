import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { LocationService } from 'src/app/services/location.services';

@Component({
  selector: 'app-locations',
  templateUrl: './locations.page.html',
  styleUrls: ['./locations.page.scss'],
})
export class LocationsPage implements OnInit {

  errorMessage: string = '';
  credentials: FormGroup;
  showList = true;
  listofLocations: any[] = []
  constructor(
    private fb: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public router: Router,
    public locationService: LocationService
  ) { }

  ngOnInit() {
    this.getLocation();
    this.credentials = this.fb.group({
      location: ['', [Validators.required]],
      country: ['', [Validators.required]],
      state: ['', [Validators.required]],
      district: ['', [Validators.required]],
      pincode: ['', []],
    });
  }
  addNew(i) {
    if (i === 1) {
      this.showList = false;
    } else {
      this.showList = true;
    }
  }
  get location() {
    return this.credentials.get('location');
  }
  get country() {
    return this.credentials.get('country');
  }
  get state() {
    return this.credentials.get('state');
  }
  get district() {
    return this.credentials.get('district');
  }
  get pincode() {
    return this.credentials.get('pincode');
  }
  async addNewLocation() {
    const loading = await this.loadingController.create();
    await loading.present();
    console.log(this.credentials.value);
    this.errorMessage = '';
    this.locationService.createLocations(this.credentials.value).subscribe(
      async (res) => {
        console.log(res);
        await loading.dismiss();
        this.credentials.reset();
        this.showList = true;
        this.getLocation();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );

  }
  async getLocation() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    this.locationService.getLocations().subscribe(
      async (res) => {
        console.log(res);
        this.listofLocations = res.data;
        await loading.dismiss();

      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );

  }
}
