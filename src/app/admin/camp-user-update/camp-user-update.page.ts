import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { PrayerGroupService } from 'src/app/services/prayer-group.services';
import { RolesService } from 'src/app/services/roles.services';
import { UsersService } from 'src/app/services/users.services';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AttendeeStatus } from 'src/app/services/API';
import { id } from 'date-fns/locale';
import { PrayersService } from 'src/app/services/prayers.services';

@Component({
  selector: 'app-camp-user-update',
  templateUrl: './camp-user-update.page.html',
  styleUrls: ['./camp-user-update.page.scss'],
})
export class CampUserUpdatePage implements OnInit {
  item: any;
  credentials!: FormGroup;
  prayerGroups: any[] = [];
  staffInfo: any = {};
  userInfo: any = {};
  constructor(
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router,
    private navController: NavController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public usersService: UsersService,
    public authService: AuthenticationService,
    public rolesService: RolesService,
    public prayerGroupService: PrayerGroupService,
    public prayersService: PrayersService) {
    this.item = {};
    this.route.queryParams.subscribe(_p => {
      const navParams = this.router.getCurrentNavigation().extras.state;
      if (navParams) {
        this.item = navParams.item;
        console.log(this.item);
      };
    });
    this.getPrayerGroups();
    this.authService.userInfo.subscribe((res) => {
      this.userInfo = res;
    });
  }
  get prayerGroupId(): FormControl {
    return this.credentials.get('prayerGroupId') as FormControl;
  }
  get place(): FormControl {
    return this.credentials.get('place') as FormControl;
  }
  get address(): FormControl {
    return this.credentials.get('address') as FormControl;
  }
  get phoneNumber(): FormControl {
    return this.credentials.get('phoneNumber') as FormControl;
  }
  get refrence(): FormControl {
    return this.credentials.get('refrence') as FormControl;
  }
  get numberOfCampsAttended(): FormControl {
    return this.credentials.get('numberOfCampsAttended') as FormControl;
  }
  get payment(): FormControl {
    return this.credentials.get('payment') as FormControl;
  }
  get amount(): FormControl {
    return this.credentials.get('amount') as FormControl;
  }
  get isJvdmMember(): FormControl {
    return this.credentials.get('isJvdmMember') as FormControl;
  }
  get idCard(): FormControl {
    return this.credentials.get('idCard') as FormControl;
  }
  get memberSince(): FormControl {
    return this.credentials.get('memberSince') as FormControl;
  }
  ngOnInit() {
    this.credentials = this.fb.group({
      prayerGroupId: ['', Validators.required],
      phoneNumber: [0, Validators.required],
      place: [''],
      address: [''],
      refrence: [''],
      gender: ['male', Validators.required],
      idCard: ['', Validators.required],
      isChild: [false, Validators.required],
      isPaid: [true, Validators.required],
      campCount: [0],
      memberSince: ['', Validators.required],
      isJvdmMember: [false],
      amount: ['', Validators.required]
    });
  }
  async updateAttenddee(value) {
    console.log(value);
    const attendee = {
      hasMembers: this.item.hasMembers,
      isSmsSend: true,
      idCard: value.idCard,
      campCount: value.campCount,
      isChild: value.isChild,
      prayerGroupId: value.prayerGroupId.id,
      isJvdmMember: value.isJvdmMember,
      memberSince: value.memberSince,
      isPaid: value.isPaid,
      amount: value.amount,
      status: AttendeeStatus.REGISTERED,
      meetingId: this.item.meetingId,
      userId: this.item.userId,
      staffId: this.userInfo.id,
    };
    console.log(attendee);
    const loading = await this.loadingController.create();
    await loading.present();
    this.prayersService.updateAttendeese(attendee, this.item.id).subscribe(
      async (res) => {
        await loading.dismiss();
        if (res.code === 200) {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Success',
            message: 'Registration Completed Successfully',
            buttons: ['OK']
          });
          await alert.present();
          this.navController.back();
        } else {
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Error',
            message: 'Something went wrong. Please try again',
            buttons: ['OK']
          });
          await alert.present();
        }
      },
      async (error) => {
        await loading.dismiss();
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Error',
          message: 'Something went wrong. Please try again',
          buttons: ['OK']
        });
        await alert.present();
      });
  }
  async getPrayerGroups() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.prayerGroupService.getPrayerGroups().subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.prayerGroups = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }

}
