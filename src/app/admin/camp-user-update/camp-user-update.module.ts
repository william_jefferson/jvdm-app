import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CampUserUpdatePageRoutingModule } from './camp-user-update-routing.module';

import { CampUserUpdatePage } from './camp-user-update.page';
import { IonicSelectableModule } from 'ionic-selectable';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    IonicSelectableModule,
    CampUserUpdatePageRoutingModule
  ],
  declarations: [CampUserUpdatePage]
})
export class CampUserUpdatePageModule {}
