import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampUserUpdatePage } from './camp-user-update.page';

const routes: Routes = [
  {
    path: '',
    component: CampUserUpdatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampUserUpdatePageRoutingModule {}
