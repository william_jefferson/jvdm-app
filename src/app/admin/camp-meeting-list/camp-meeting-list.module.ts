import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CampMeetingListPageRoutingModule } from './camp-meeting-list-routing.module';

import { CampMeetingListPage } from './camp-meeting-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CampMeetingListPageRoutingModule
  ],
  declarations: [CampMeetingListPage]
})
export class CampMeetingListPageModule {}
