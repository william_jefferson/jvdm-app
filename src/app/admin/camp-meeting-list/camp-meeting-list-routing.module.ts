import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampMeetingListPage } from './camp-meeting-list.page';

const routes: Routes = [
  {
    path: '',
    component: CampMeetingListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampMeetingListPageRoutingModule {}
