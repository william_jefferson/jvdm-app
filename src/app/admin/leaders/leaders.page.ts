import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { Roles } from 'src/app/services/Constants';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-leaders',
  templateUrl: './leaders.page.html',
  styleUrls: ['./leaders.page.scss'],
})
export class LeadersPage implements OnInit {
  leaders: any[] = [];
  constructor(
    public loadingController: LoadingController,
    public alertController: AlertController,
    private userService: UsersService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.getLeaders();
  }
  async getLeaders() {
    const loading = await this.loadingController.create();
    await loading.present();
    const leaderParam = 'roleId=' + Roles.Leader;
    this.userService.getUsers(leaderParam).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.leaders = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }
}
