import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CampRegistrationPageRoutingModule } from './camp-registration-routing.module';
import { CampRegistrationPage } from './camp-registration.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CampRegistrationPageRoutingModule
  ],
  declarations: [CampRegistrationPage]
})
export class CampRegistrationPageModule {}
