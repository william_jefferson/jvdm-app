import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BarcodeScanner } from '@awesome-cordova-plugins/barcode-scanner/ngx';
import { LoadingController, AlertController, NavController } from '@ionic/angular';
import { AttendeeStatus } from 'src/app/services/API';
import { PrayersService } from 'src/app/services/prayers.services';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-camp-registration',
  templateUrl: './camp-registration.page.html',
  styleUrls: ['./camp-registration.page.scss'],
})
export class CampRegistrationPage implements OnInit {
  members: any[] = [];
  phonenumber: any;
  barcodeId: any;

  constructor(
    public loadingController: LoadingController,
    public alertController: AlertController,
    private userService: UsersService,
    public router: Router,
    public prayersService: PrayersService,
    private nav: NavController,
    private barcodeScanner: BarcodeScanner) { }

  ngOnInit() {
  }
  ionViewDidEnter() {
    if(this.phonenumber){
      this.getAttendees('phoneNumber=' + this.phonenumber);
    }else if(this.barcodeId){
      this.getAttendees('recepitCode=' + this.barcodeId );
    }
  }
  async getAttendees(params: any) {
    const loading = await this.loadingController.create();
    await loading.present();

    const scanParams = '?status=' + AttendeeStatus.PRE_REGISTERED + '&meetingId=4&' + params;
    this.prayersService.getAttendees(scanParams).subscribe(
      async (res) => {
        await loading.dismiss();
        if (res.code === 200) {
          console.log(res);
          if (res.data.length > 0) {
            this.members = res.data;
          } else {
            const alert = await this.alertController.create({
              cssClass: 'my-custom-class',
              header: 'Error',
              message: 'Sorry, No Registerd Record Found',
              buttons: ['OK']
            });
            await alert.present();
          }

        }
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }
  async searchByPhoneNumber() {
    if (this.phonenumber.length === 10) {
      this.getAttendees('phoneNumber=' + this.phonenumber);
    } else {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Error',
        message: 'Please enter 10 digit number',
        buttons: ['OK']
      });
      await alert.present();
    }
  }
  updateDetails(item) {
    this.nav.navigateForward(`camp-user-update`, { state: { item } });
  }
  async scanQRCode() {
    this.barcodeScanner.scan().then(async barcodeData => {
      console.log('Barcode data', barcodeData);
      if (barcodeData.cancelled === false) {
        this.barcodeId = barcodeData.text.replace('https://jesusvision.in/megacamp-registration/', '');
        this.getAttendees('recepitCode=' + this.barcodeId );
      } else {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: 'Error',
          message: 'Qr Code could not found',
          buttons: ['OK']
        });
        await alert.present();
      }
      //;
    }).catch(async err => {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Error',
        message: 'Qr Code could not found',
        buttons: ['OK']
      });
      await alert.present();
    });
  }

}
