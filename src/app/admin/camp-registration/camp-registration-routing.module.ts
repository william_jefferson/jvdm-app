import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CampRegistrationPage } from './camp-registration.page';

const routes: Routes = [
  {
    path: '',
    component: CampRegistrationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CampRegistrationPageRoutingModule {}
