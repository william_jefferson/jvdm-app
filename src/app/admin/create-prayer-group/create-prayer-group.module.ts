import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreatePrayerGroupPageRoutingModule } from './create-prayer-group-routing.module';

import { CreatePrayerGroupPage } from './create-prayer-group.page';
import { IonicSelectableModule } from 'ionic-selectable';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    CreatePrayerGroupPageRoutingModule
  ],
  declarations: [CreatePrayerGroupPage]
})
export class CreatePrayerGroupPageModule {}
