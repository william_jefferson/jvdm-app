import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { Roles } from 'src/app/services/Constants';
import { LocationService } from 'src/app/services/location.services';
import { PrayerGroupService } from 'src/app/services/prayer-group.services';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-create-prayer-group',
  templateUrl: './create-prayer-group.page.html',
  styleUrls: ['./create-prayer-group.page.scss'],
})
export class CreatePrayerGroupPage implements OnInit {
  errorMessage: string = '';
  credentials: FormGroup;
  selectedLeader: any;
  selectedLocation: any;
  leaders: any[];
  locations: any[];
  constructor(
    private fb: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private prayerGroupService: PrayerGroupService,
    private locationService: LocationService,
    private userService: UsersService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.getLeaders();
    this.getLocation();
    this.credentials = this.fb.group({
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
      leaderId: ['', [Validators.required]],
      locationId: ['', [Validators.required]],
    });
  }
  get name() {
    return this.credentials.get('name');
  }
  get description() {
    return this.credentials.get('description');
  }
  get leaderId() {
    return this.credentials.get('leaderId');
  }
  get locationId() {
    return this.credentials.get('locationId');
  }
  addLocation() {
    this.router.navigateByUrl('/locations', { replaceUrl: true });
  }
  async addPrayerGroup() {
    const loading = await this.loadingController.create();
    await loading.present();
    console.log(this.credentials.value);
    this.errorMessage = '';
    const prayerGroup = {
      "name": this.credentials.get('name').value,
      "description": this.credentials.get('description').value,
      "leaderId":this.credentials.get('leaderId').value.id,
      "locationId":this.credentials.get('locationId').value.id,
    };
    this.prayerGroupService.createPrayerGroups(prayerGroup).subscribe(
      async (res) => {
        console.log(res);
        await loading.dismiss();
        this.credentials.reset();
        this.router.navigateByUrl('/prayer-groups',{replaceUrl: true});
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );

  }
  async getLocation() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    this.locationService.getLocations().subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.locations = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
  async leaderChange(ev) {
    console.log(ev);
  }
  async locationChange(ev) {
    console.log(ev);
  }
  async getLeaders() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    const leaderParam = 'roleId=' + Roles.Leader;
    this.userService.getUsers(leaderParam).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.leaders = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
}
