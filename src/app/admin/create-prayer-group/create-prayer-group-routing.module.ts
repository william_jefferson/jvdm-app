import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreatePrayerGroupPage } from './create-prayer-group.page';

const routes: Routes = [
  {
    path: '',
    component: CreatePrayerGroupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreatePrayerGroupPageRoutingModule {}
