import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrayerGroupsPageRoutingModule } from './prayer-groups-routing.module';

import { PrayerGroupsPage } from './prayer-groups.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrayerGroupsPageRoutingModule
  ],
  declarations: [PrayerGroupsPage]
})
export class PrayerGroupsPageModule {}
