import { Component, OnInit } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { PrayerGroupService } from 'src/app/services/prayer-group.services';

@Component({
  selector: 'app-prayer-groups',
  templateUrl: './prayer-groups.page.html',
  styleUrls: ['./prayer-groups.page.scss'],
})
export class PrayerGroupsPage implements OnInit {
  prayerGroups: any[] = [];
  constructor(
    public loadingController: LoadingController,
    public alertController: AlertController,
    public prayerGroupService: PrayerGroupService) { }

  ngOnInit() {
    this.getPrayerGroups();
  }
  async getPrayerGroups() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.prayerGroupService.getPrayerGroups().subscribe(
      async (res) => {
        console.log(res);
        if(res.code === 200){
          this.prayerGroups = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }
}
