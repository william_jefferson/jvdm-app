import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateSchedulesPage } from './create-schedules.page';

const routes: Routes = [
  {
    path: '',
    component: CreateSchedulesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateSchedulesPageRoutingModule {}
