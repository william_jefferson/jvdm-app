import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateSchedulesPageRoutingModule } from './create-schedules-routing.module';

import { CreateSchedulesPage } from './create-schedules.page';
import { IonicSelectableModule } from 'ionic-selectable';
import { CounclingHistoryComponent } from 'src/app/modal/councling-history/councling-history.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    IonicSelectableModule,
    CreateSchedulesPageRoutingModule
  ],
  declarations: [CreateSchedulesPage, CounclingHistoryComponent]
})
export class CreateSchedulesPageModule {}
