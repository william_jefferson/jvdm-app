import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { Roles } from 'src/app/services/Constants';
import { PrayerGroupService } from 'src/app/services/prayer-group.services';
import { RolesService } from 'src/app/services/roles.services';
import { UsersService } from 'src/app/services/users.services';
import { CounsellingService } from 'src/app/services/counselling.services';
import { CounclingHistoryComponent } from 'src/app/modal/councling-history/councling-history.component';
import { AuthenticationService } from 'src/app/services/authentication.service';
@Component({
  selector: 'app-create-schedules',
  templateUrl: './create-schedules.page.html',
  styleUrls: ['./create-schedules.page.scss'],
})
export class CreateSchedulesPage implements OnInit {
  newUser = true;
  prayerGroups: any[] = [];
  counslingMembers: any[] = [];
  userMembers: any[] = [];
  credentials: FormGroup;
  credentialsPreUser: FormGroup;
  errorMessage = '';
  roles: any[] = [];
  userInfo: any;
  counsellorInfo: any;
  isAdmin = false;
  isManager = false;
  constructor(
    private fb: FormBuilder,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public usersService: UsersService,
    public rolesService: RolesService,
    public prayerGroupService: PrayerGroupService,
    public counsellingService: CounsellingService,
    public authService: AuthenticationService,
    public router: Router
  ) {
    this.authService.isAdmin.subscribe((res)=>{
      this.isAdmin = res;
    });
    this.authService.isManager.subscribe((res)=>{
      this.isManager = res;
    });
   }

  ngOnInit() {
    this.getPrayerGroups();
    this.getCounslingMembers();
    this.getMembers();
    this.credentials = this.fb.group({
      fullName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      phonenumber:['', [Validators.required]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      roleId: [Roles.Member, [Validators.required]],
      scheduleDateTime : [, [Validators.required]],
      counsellor: [, [Validators.required]],
      prayergroup: [, [Validators.required]],
      notes: [],
    });
    this.credentialsPreUser = this.fb.group({
      userId: [, [Validators.required]],
      scheduleDateTime : [, [Validators.required]],
      counsellor: [, [Validators.required]],
      prayergroup: [, [Validators.required]],
      notes: [],
    });
  }
  async presentHistoryModal(type,id) {
    const modal = await this.modalController.create({
      component: CounclingHistoryComponent,
      cssClass: 'histroryModal',
      swipeToClose: true,
      componentProps: {
        userType: type,
        userId: id,
      }
    });
    return await modal.present();
  }
  counsellorSelected(ev,i){
    if(i === 1){
      this.counsellorInfo = this.credentialsPreUser.get('counsellor')?.value;
    }else{
      this.counsellorInfo = this.credentials.get('counsellor')?.value;
    }
  }
  addNew(i) {
    console.log(i);
    this.counsellorInfo = null;
    this.credentialsPreUser.get('counsellor').setValue(null);
    this.credentials.get('counsellor').setValue(null);
    if (i === 1) {
      this.newUser = true;
    } else {
      this.newUser = false;
    }
  }
  filterUsers(userMembers: any[], text: string) {
    return userMembers.filter(userMember => userMember.fullName.toLowerCase().indexOf(text) !== -1 ||
      userMember.phone.toLowerCase().indexOf(text) !== -1);
  }
  userSelected(event){
    this.userInfo = this.credentialsPreUser.get('userId')?.value;
  }
  userSearch(event){
    event.component.startSearch();
    const text = event.text.trim().toLowerCase();
    event.component.items = this.filterUsers(this.userMembers, text);
    event.component.endSearch();
  }
  async register() {
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    const register = {
      fullName: this.credentials.get('fullName')?.value,
      email: this.credentials.get('email')?.value,
      username: this.credentials.get('email')?.value,
      phone:this.credentials.get('email')?.value,
      password: this.credentials.get('phonenumber')?.value,
      userRole:[{
        roleId:this.credentials.get('roleId')?.value
    }]
    };
    console.log(register);
     this.usersService.createUser(register).subscribe(
      async (res) => {
        console.log(res);
        if(res.code === 200){
          this.userInfo = res.data;
          await loading.dismiss();
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Success',
            message: 'User Created Successfully',
            buttons: ['OK']
          });
          this.createSchedule();
          await alert.present();
          const { role } = await alert.onDidDismiss();
          console.log('onDidDismiss resolved with role', role);
        }

      },
      async (res) => {
        console.log(res);

        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
  async createSchedule(){
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    const register = {
      userId: this.userInfo.id,
      counselorId:this.credentials.get('counsellor').value.id,
      prayerGroupId: this.credentials.get('prayergroup').value.id,
      note: this.credentials.get('notes').value,
      assignDate:this.credentials.get('scheduleDateTime').value
    };
    console.log(register);
     this.counsellingService.createCounselling(register).subscribe(
      async (res) => {
        console.log(res);
        if(res.code === 200){
          this.credentials.reset();
          await loading.dismiss();
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Success',
            message: 'Counceling Scheduled successfully',
            buttons: ['OK']
          });
          await alert.present();
          const { role } = await alert.onDidDismiss();
          console.log('onDidDismiss resolved with role', role);
        }

      },
      async (res) => {
        console.log(res);

        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
  async createSchedulePre(){
    const loading = await this.loadingController.create();
    await loading.present();
    this.errorMessage = '';
    const register = {
      userId: this.userInfo.id,
      counselorId:this.credentialsPreUser.get('counsellor').value.id,
      prayerGroupId: this.credentialsPreUser.get('prayergroup').value.id,
      note: this.credentialsPreUser.get('notes').value,
      assignDate:this.credentialsPreUser.get('scheduleDateTime').value
    };
    console.log(register);
     this.counsellingService.createCounselling(register).subscribe(
      async (res) => {
        console.log(res);
        if(res.code === 200){
          this.credentialsPreUser.reset();
          await loading.dismiss();
          const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: 'Success',
            message: 'Counceling Scheduled successfully',
            buttons: ['OK']
          });
          await alert.present();
          const { role } = await alert.onDidDismiss();
          console.log('onDidDismiss resolved with role', role);
        }

      },
      async (res) => {
        console.log(res);

        if (res.error.code === 400) {
          this.errorMessage = res.error.message;
        }
        await loading.dismiss();
      }
    );
  }
  async getPrayerGroups() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.prayerGroupService.getPrayerGroups().subscribe(
      async (res) => {
        console.log(res);
        if(res.code === 200){
          this.prayerGroups = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        await loading.dismiss();
      }
    );

  }
  async getCounslingMembers() {
    const loading = await this.loadingController.create();
    await loading.present();
    const userParam = 'roleId=' + Roles.Counselor;
    this.usersService.getUsers(userParam).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.counslingMembers = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }
  async getMembers() {
    const loading = await this.loadingController.create();
    await loading.present();
    const userParam = 'roleId=' + Roles.Member;
    this.usersService.getUsers(userParam).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.userMembers = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }
  get fullName() {
    return this.credentials.get('fullName');
  }
  get email() {
    return this.credentials.get('email');
  }
  get roleId() {
    return this.credentials.get('roleId');
  }
  get password() {
    return this.credentials.get('password');
  }
  get counsellor() {
    return this.credentials.get('counsellor');
  }
  get prayergroup() {
    return this.credentials.get('prayergroup');
  }
  get phonenumber() {
    return this.credentials.get('phonenumber');
  }
  get scheduleDateTime() {
    return this.credentials.get('scheduleDateTime');
  }
  get notes() {
    return this.credentials.get('notes');
  }
}
