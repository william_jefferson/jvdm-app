import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController, ActionSheetController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Roles, ScheduleStatus } from 'src/app/services/Constants';
import { CounsellingService } from 'src/app/services/counselling.services';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.page.html',
  styleUrls: ['./schedules.page.scss'],
})
export class SchedulesPage implements OnInit {

  bookings: any[] = [];
  queryparams = '';
  isAdmin = false;
  isManager = false;
  isCouncelor = false;
  isLeader = false;
  constructor(
    public loadingController: LoadingController,
    public alertController: AlertController,
    private userService: UsersService,
    private authService: AuthenticationService,
    private actionSheetController: ActionSheetController,
    private councsellingService: CounsellingService,
    public router: Router,
  ) {

    this.authService.isAdmin.subscribe((res) => {
      this.isAdmin = res;
    });
    this.authService.isManager.subscribe((res) => {
      this.isManager = res;
    });
    this.authService.isLeader.subscribe((res) => {
      this.isLeader = res;
    });
    this.authService.isCouncelor.subscribe((res) => {
      this.isCouncelor = res;
    });
  }

  ngOnInit() {
    if (this.authService.isAdmin.value) {
      this.queryparams = '';
      this.getCounselling(this.queryparams);
    } else if (this.authService.isLeader.value) {
      this.queryparams = 'leaderId=' + this.authService.userInfo.value.id;
      this.getCounselling(this.queryparams);
    } if (this.authService.isCouncelor.value) {
      this.queryparams = 'counselorId=' + this.authService.userInfo.value.id;
      this.getCounselling(this.queryparams);
    }
  }
  doRefresh(event) {
    this.getCounselling(this.queryparams)
    setTimeout(() => {
      event.target.complete();
    }, 2000);
  }

  async updateStatus() {
    const alert = await this.alertController.create({
      header: "",
      cssClass: 'my-custom-class',
      message: '<div class="text-center"><ion-icon class="alert-success" color="primary"  name="checkmark-circle"></ion-icon> <br/>'
        + "" + '</div>',
      buttons: ['OK'],
    });
    await alert.present();
  }

  async getCounselling(requestParams) {
    const loading = await this.loadingController.create();
    await loading.present();

    this.councsellingService.getCounselling(requestParams).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.bookings = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }
  async callCouncelor(booking) {
    console.log(booking);

  }
  async presentScheduleStatusUpdate(booking) {
    const btnPending = {
      text: ScheduleStatus.pending,
      role: 'selected',
      icon: 'hourglass',
      id: ScheduleStatus.pending + 'btn',
      data: {
        type: ScheduleStatus.pending
      },
      handler: () => {
        this.updateScheduleStatus(booking,ScheduleStatus.pending);
      }
    };
    const btnCompleted = {
      text: ScheduleStatus.completed,
      role: 'selected',
      icon: 'checkmark-circle',
      id: ScheduleStatus.completed + 'btn',
      data: {
        type: ScheduleStatus.completed
      },
      handler: () => {
        this.updateScheduleStatus(booking,ScheduleStatus.completed);
      }
    };
    const btnApproved = {
      text: ScheduleStatus.approved,
      role: 'selected',
      icon: 'checkmark-circle',
      id: ScheduleStatus.approved + 'btn',
      data: {
        type: ScheduleStatus.approved
      },
      handler: () => {
        this.updateScheduleStatus(booking,ScheduleStatus.approved);
      }
    };
    const btnReSchedule = {
      text: ScheduleStatus.reschedule,
      role: 'selected',
      icon: 'calendar',
      id: ScheduleStatus.reschedule + 'btn',
      data: {
        type: ScheduleStatus.reschedule
      },
      handler: () => {
        this.updateScheduleStatus(booking,ScheduleStatus.reschedule);
      }
    };
    const btnCancelled = {
      text: ScheduleStatus.cancelled,
      role: 'destructive',
      icon: 'close-circle',
      id: ScheduleStatus.cancelled + 'btn',
      data: {
        type: ScheduleStatus.cancelled
      },
      handler: () => {
        this.updateScheduleStatus(booking,ScheduleStatus.cancelled);
      }
    };
    const btnCancel = {
      text: 'Cancel',
      icon: 'close',
      role: 'cancel',
      handler: () => {
        console.log('Cancel clicked');
      }
    };
    let statusBtn = [];
    if (this.isAdmin || this.isManager) {
      statusBtn.push(btnPending);
      statusBtn.push(btnCompleted);
      statusBtn.push(btnApproved);
      statusBtn.push(btnReSchedule);
      statusBtn.push(btnCancelled);
      statusBtn.push(btnCancel);
    }
    if (this.isLeader) {
      statusBtn.push(btnCompleted);
      statusBtn.push(btnApproved);
      statusBtn.push(btnReSchedule);
      statusBtn.push(btnCancelled);
      statusBtn.push(btnCancel);
    }
    if (this.isCouncelor) {
      statusBtn.push(btnCompleted);
      statusBtn.push(btnReSchedule);
      statusBtn.push(btnCancelled);
      statusBtn.push(btnCancel);
    }
    const actionSheet = await this.actionSheetController.create({
      header: 'Update Current Status to',
      cssClass: 'my-custom-class',
      mode: 'ios',
      buttons: statusBtn
    });
    await actionSheet.present();

    const { role, data } = await actionSheet.onDidDismiss();
    console.log('onDidDismiss resolved with role and data', role, data);
  }
  async updateScheduleStatus(booking,updateStatus) {
    const loading = await this.loadingController.create();
    await loading.present();
    const status = {
      status: updateStatus
    }
    this.councsellingService.updateCounseling(booking.id,status).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.getCounselling(this.queryparams);
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }
}