import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, AlertController } from '@ionic/angular';
import { Roles } from 'src/app/services/Constants';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-counsling-members',
  templateUrl: './counsling-members.page.html',
  styleUrls: ['./counsling-members.page.scss'],
})
export class CounslingMembersPage implements OnInit {

  counslingMembers: any[] = [];
  constructor(
    public loadingController: LoadingController,
    public alertController: AlertController,
    private userService: UsersService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.getCounslingMembers();
  }
  async getCounslingMembers() {
    const loading = await this.loadingController.create();
    await loading.present();
    const userParam = 'roleId=' + Roles.Counselor;
    this.userService.getUsers(userParam).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.counslingMembers = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }


}
