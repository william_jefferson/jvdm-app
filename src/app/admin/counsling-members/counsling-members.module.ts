import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CounslingMembersPageRoutingModule } from './counsling-members-routing.module';

import { CounslingMembersPage } from './counsling-members.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CounslingMembersPageRoutingModule
  ],
  declarations: [CounslingMembersPage]
})
export class CounslingMembersPageModule {}
