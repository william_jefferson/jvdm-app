import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CounslingMembersPage } from './counsling-members.page';

const routes: Routes = [
  {
    path: '',
    component: CounslingMembersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CounslingMembersPageRoutingModule {}
