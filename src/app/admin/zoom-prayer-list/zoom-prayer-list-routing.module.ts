import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ZoomPrayerListPage } from './zoom-prayer-list.page';

const routes: Routes = [
  {
    path: '',
    component: ZoomPrayerListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoomPrayerListPageRoutingModule {}
