import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ZoomPrayerListPageRoutingModule } from './zoom-prayer-list-routing.module';

import { ZoomPrayerListPage } from './zoom-prayer-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ZoomPrayerListPageRoutingModule
  ],
  declarations: [ZoomPrayerListPage]
})
export class ZoomPrayerListPageModule {}
