import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CoordinatorsPageRoutingModule } from './coordinators-routing.module';

import { CoordinatorsPage } from './coordinators.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CoordinatorsPageRoutingModule
  ],
  declarations: [CoordinatorsPage]
})
export class CoordinatorsPageModule {}
