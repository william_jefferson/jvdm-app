import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CoordinatorsPage } from './coordinators.page';

const routes: Routes = [
  {
    path: '',
    component: CoordinatorsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CoordinatorsPageRoutingModule {}
