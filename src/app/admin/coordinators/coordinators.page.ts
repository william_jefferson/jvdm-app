import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, LoadingController } from '@ionic/angular';
import { Roles } from 'src/app/services/Constants';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-coordinators',
  templateUrl: './coordinators.page.html',
  styleUrls: ['./coordinators.page.scss'],
})
export class CoordinatorsPage implements OnInit {

  coordinators: any[] = [];
  constructor(
    public loadingController: LoadingController,
    public alertController: AlertController,
    private userService: UsersService,
    public router: Router,
  ) { }

  ngOnInit() {
    this.getCoordinators();
  }
  async getCoordinators() {
    const loading = await this.loadingController.create();
    await loading.present();
    const userParam = 'roleId=' + Roles.Coordinator;
    this.userService.getUsers(userParam).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.coordinators = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }

}
