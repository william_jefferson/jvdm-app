/* eslint-disable @typescript-eslint/naming-convention */

export class Constants {
    public static TOKEN_KEY: any = 'my-token';
    public static LANG_KEY: any = 'lang';
    public static USER_KEY: any = 'my-data';
    public static USER_ID: any = 'userId';
    public static USER_ROLES: any = 'roles';
}
export enum Notification {
    LINK = 'link',
    YOUTUBE = 'youtube',
    ALERT = 'alert',
    PRAYER = 'prayer',
    ANNOUNCEMENT = 'announcement',
    IMAGE = 'image',
}
export enum Roles {
    Admin = 1,
    Manager = 2,
    Leader = 3,
    Coordinator = 4,
    Counselor = 5,
    Member = 6
}
export enum ScheduleStatus {
    completed = 'completed',
    approved = 'approved',
    pending = 'pending',
    cancelled = 'cancelled',
    reschedule = 'reschedule',
}