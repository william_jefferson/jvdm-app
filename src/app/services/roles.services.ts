import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from './API';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class RolesService {

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) { }

  getRole(): Observable<any> {
    return this.http.get(API.getRoles)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getRoles', [])
        ));
  }
 
 
  // createCase(request: any,requestId): Observable<any> {
  //   return this.http.post(API.createCases+'?requestTypeId='+requestId, request)
  //     .pipe(
  //       catchError(
  //         this.errorHandlerService.handleError<any>('createCase', [])
  //       ));
  // }
}
