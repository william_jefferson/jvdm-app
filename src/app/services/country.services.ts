import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from './API';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) { }

  getCountries(): Observable<any> {
    return this.http.get(API.countries)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getCountries', [])
        ));
  }
  createCountries(request: any): Observable<any> {
    return this.http.post(API.countries, request)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('createCountries', [])
        ));
  }
}
