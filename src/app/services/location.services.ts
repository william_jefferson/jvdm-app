import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from './API';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) { }

  getLocations(): Observable<any> {
    return this.http.get(API.locations)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getLocations', [])
        ));
  }
  createLocations(request: any): Observable<any> {
    return this.http.post(API.locations, request)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('createLocations', [])
        ));
  }
}
