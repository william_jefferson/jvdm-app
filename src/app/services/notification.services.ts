import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from './API';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  
  newnotification: BehaviorSubject<Boolean> = new BehaviorSubject<Boolean>(null);

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) { }

  getNotifications(): Observable<any> {
    return this.http.get(API.notifications)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getNotifications', [])
        ));
  }
 
  // createCase(request: any,requestId): Observable<any> {
  //   return this.http.post(API.createCases+'?requestTypeId='+requestId, request)
  //     .pipe(
  //       catchError(
  //         this.errorHandlerService.handleError<any>('createCase', [])
  //       ));
  // }
}
