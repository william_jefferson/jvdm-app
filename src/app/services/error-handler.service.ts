import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingController, ToastController } from '@ionic/angular';
import { Observable, of } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
@Injectable({
  providedIn: 'root',
})
export class ErrorHandlerService {
  generalMessage: any;
  generalLoginagain: any;
  dismiss: any;
  errorOccurd: any;
  loading: any;
  ok: any;
  success: any;
  loadingToast: any;
  safeSvg;
  constructor(
    public toastController: ToastController,
    private sanitizer: DomSanitizer,
    private loadingController: LoadingController,
    private authenticationService: AuthenticationService) {
    
    this.safeSvg = sanitizer.bypassSecurityTrustUrl('assets/img/loading.svg');
  }
 
  async presentToast(errorMessage: string) {
    const toast = await this.toastController.create({
      header: this.errorOccurd,
      message: errorMessage,
      duration: 2000,
      color: 'danger',
      buttons: [
        {
          icon: 'bug',
          text: this.dismiss,
          role: 'cancel',
        },
      ],
    });
    toast.present();
  }
  async doLoading() {
    const loading = this.loadingController.create({
      message: '<ion-img src="/assets/img/logo.png" style="background:transparent" height="10" alt="loading..."></ion-img>',
      cssClass: 'scale-down-center',
      translucent: true,
      showBackdrop: false,
      spinner: null,
    });
    (await loading).present();

    setTimeout(async () => {
       (await loading).dismiss();
    }, 3000);
  }
  async presentLoadingToast() {
    this.loadingToast = await this.loadingController.create({
      message: '<ion-img src="/assets/img/logo.png" style="background:transparent" height="10" alt="loading..."></ion-img>',
      cssClass: 'scale-down-center',
      translucent: true,
      showBackdrop: false,
      spinner: null,
    });
    this.loadingToast.present();
  }
  async dismissLoadingToast() {
    this.loadingToast.dismiss();
  }
  async presentSuccessToast(messageSuccess: string) {
    const toast = await this.toastController.create({
      header: this.errorOccurd,
      message: messageSuccess,
      duration: 2000,
      color: 'btnGreen',
      buttons: [
        {
          icon: 'checkmark-outline',
          text: this.ok,
          role: 'cancel',
        },
      ],
    });
    toast.present();
  }
  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      if (error.status === 401) {
        this.authenticationService.logout();
        return of(result as T).pipe(tap(() => this.presentToast(this.generalLoginagain)));
      } else if (error.status === -1) {
        return of(result as T).pipe(tap(() => this.presentToast(this.generalMessage)));
      } else {
        console.warn(`${operation} failed: ${error.message}`);
        return of(result as T).pipe(tap(() => this.presentToast(this.generalMessage)));
      }
    };
  }
}
