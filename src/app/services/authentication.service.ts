import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { API } from './API';
import { Constants } from './Constants';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  // Init with null to filter out the first value in a guard!
  isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  isAdmin: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  isManager: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  isLeader: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  isCouncelor: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  userInfo: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  token = '';

  constructor(private http: HttpClient) {
    this.loadToken();
  }

  async loadToken() {
    const token = window.localStorage.getItem(Constants.TOKEN_KEY);
    if (token) {
      console.log('set token: ', token);
      this.token = token;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
    const userInfo = JSON.parse(window.localStorage.getItem(Constants.USER_KEY));

    if (userInfo) {
      console.log('set userInfo: ', userInfo);
      this.userInfo.next(userInfo);
    }
  }

  login(credentials): Observable<any> {
    return this.http.post(API.login, credentials).pipe(
      map((data: any) => data),
      // eslint-disable-next-line arrow-body-style
      switchMap(data => {
        if (data.code === 200) {
          const userIo = data.data.user;
          this.userInfo.next(userIo);
          window.localStorage.setItem(Constants.TOKEN_KEY, data.data.token);
          window.localStorage.setItem(Constants.USER_KEY, JSON.stringify(userIo));
          this.isAuthenticated.next(true);
          return of(data);
        } else {
          return of(data);
        }
      }),
      tap(_ => {
      })
    );
  }
  register(credentials): Observable<any> {
    return this.http.post(API.register, credentials).pipe(
      map((data: any) => data),
      // eslint-disable-next-line arrow-body-style
      switchMap(data => {
        if (data.code === 200) {
          const userIo = data.data.user;
          this.userInfo.next(userIo);
          window.localStorage.setItem(Constants.TOKEN_KEY, data.data.token);
          window.localStorage.setItem(Constants.USER_KEY, JSON.stringify(userIo));
          this.isAuthenticated.next(true);
          return of(data);
        } else {
          return of(data);
        }
      }),
      tap(_ => {
      })
    );
  }
  deleteAccount(id): Observable<any>{
    return this.http.delete(API.users+'/'+id).pipe(
      map((data: any) => data),
      // eslint-disable-next-line arrow-body-style
      switchMap(data => {
        if (data.code === 200) {
          this.isAuthenticated.next(false);
          return of(data);
        } else {
          return of(data);
        }
      }),
      tap(_ => {
      })
    );
  }
  logout(): Promise<void> {
    return new Promise(resolve => {
      this.isAuthenticated.next(false);
      this.userInfo.next(null);
      window.localStorage.removeItem(Constants.USER_KEY);
      window.localStorage.removeItem(Constants.TOKEN_KEY);
      resolve();
    });

  }

}
