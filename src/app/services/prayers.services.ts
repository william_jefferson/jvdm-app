import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from './API';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class PrayersService {

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) { }

  getWeeklyPrayers(): Observable<any> {
    return this.http.get(API.prayers+'?isCamp=false')
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getLocations', [])
        ));
  }
  createLocations(request: any): Observable<any> {
    return this.http.post(API.locations, request)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('createLocations', [])
        ));
  }
  getAttendees(params: string): Observable<any> {
    return this.http.get(API.attendees+params)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getAttendees', [])
        ));
  }
  updateAttendeese(request: any ,id: number): Observable<any> {
    return this.http.patch(API.attendees+'/'+id, request);
  }
}
