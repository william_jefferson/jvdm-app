import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';
import { API } from './API';
import { AuthenticationService } from './authentication.service';
import { Constants } from './Constants';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient, 
    private authService : AuthenticationService,
    private errorHandlerService: ErrorHandlerService) { }

  createUser(credentials): Observable<any> {
    return this.http.post(API.users, credentials).pipe(
      map((data: any) => data),
      // eslint-disable-next-line arrow-body-style
      switchMap(data => {
        if (data.code === 200) {
          return of(data);
        } else {
          return of(data);
        }
      }),
      tap(_ => {
      })
    );
  }
  getUsers(requestParams): Observable<any> {
    let url = API.users;
    if (requestParams) {
      url = url + '?' + requestParams;
    }
    return this.http.get(url)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getRoles', [])
        ));
  } 
  updateUserInfo(requestParams, requestData): Observable<any> {
    let url = API.users;
    if (requestParams) {
      url = url + '/' + requestParams;
    }
    return this.http.patch(url,requestData)
      .pipe(
        map((data: any) => data),
        // eslint-disable-next-line arrow-body-style
        switchMap(data => {
          if (data.code === 200) {
            const userIo = data.data;
            this.authService.userInfo.next(userIo);
            window.localStorage.setItem(Constants.USER_KEY, JSON.stringify(userIo));
            return of(data);
          } else {
            return of(data);
          }
        }),
        tap(_ => {
        }),
        catchError(
          this.errorHandlerService.handleError<any>('updateUserInfo', [])
        ));
  } 
}

