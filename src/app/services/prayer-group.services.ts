import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from './API';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class PrayerGroupService {

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) { }

  getPrayerGroups(): Observable<any> {
    return this.http.get(API.prayergroups)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getPrayerGroups', [])
        ));
  }
  createPrayerGroups(request: any): Observable<any> {
    return this.http.post(API.prayergroups, request)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('createPrayerGroups', [])
        ));
  }
  prayergroupsAssignMembers(request: any): Observable<any> {
    return this.http.post(API.prayergroupsAssignMembers, request)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('prayergroupsAssignMembers', [])
        ));
  }
  
}
