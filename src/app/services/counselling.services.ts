import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { API } from './API';
import { ErrorHandlerService } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class CounsellingService {

  constructor(private http: HttpClient, private errorHandlerService: ErrorHandlerService) { }

  getCounselling(requestParams): Observable<any> {
    let url = API.counselingBookings;
    if (requestParams) {
      url = url + '?' + requestParams;
    }
    return this.http.get(url)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('getCounselling', [])
        ));
  }
  createCounselling(request: any): Observable<any> {
    return this.http.post(API.counselingBookings, request)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('counselingBookings', [])
        ));
  }
  updateCounseling(requestId,request: any): Observable<any> {
    return this.http.patch(API.counselingBookings+'/'+requestId, request)
      .pipe(
        catchError(
          this.errorHandlerService.handleError<any>('updateCounseling', [])
        ));
  }
}
