/* eslint-disable @typescript-eslint/naming-convention */
import { environment } from 'src/environments/environment';

export class API {
    public static env: any = environment.baseApiUrl;

    public static login: any = API.env + 'authentication/login';
    public static register: any = API.env + 'authentication/signup';
    public static getRoles: any = API.env + 'roles';
    public static users: any = API.env + 'users';
    public static locations: any = API.env + 'locations';
    public static prayergroupsAssignMembers: any = API.env + 'prayer-groups/assign/members';
    public static prayergroups: any = API.env + 'prayer-groups';
    public static counselingBookings: any = API.env + 'counsellings';
    public static countries: any = API.env + 'countries';
    public static notifications: any = API.env + 'notifications';
    public static attendees: any = API.env + 'attendees';
    public static prayers: any = API.env + 'meetings/week';

}
export enum AttendeeStatus {
  PRE_REGISTERED = 'pre-registered',
  REGISTERED = 'registered',
  ATTENDED = 'attended',
  CANCELLED = 'cancelled',
};