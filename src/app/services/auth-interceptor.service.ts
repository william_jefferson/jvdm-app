import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core'; 
import { from, fromEvent, Observable, pipe } from 'rxjs';
import { filter, map, pluck, switchMap } from 'rxjs/operators';
import { Constants } from './Constants';
 

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptorService implements HttpInterceptor {
  constructor() { }
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    
    const token = window.localStorage.getItem(Constants.TOKEN_KEY);
    console.log(token);
    
    if (token) {
      const clonedRequest = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + token),
      });
      return next.handle(clonedRequest);
    }else{
      return next.handle(req);
    } 
  }
}
