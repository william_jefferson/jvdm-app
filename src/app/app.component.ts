import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { Roles } from './services/Constants';
import { PushNotifications, PushNotificationSchema, Token } from '@capacitor/push-notifications';
import { FCM } from '@capacitor-community/fcm';
import { Capacitor } from '@capacitor/core';
import { NotificationService } from './services/notification.services';
  
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  userInfo: any;
  isAdmin = false;
  isManager = false;
  isLeader = false;
  isCoordinator = false;
  isCounclingMemeber = false;
  isMember = false;
  constructor(
    public authService: AuthenticationService,
    public notificationService: NotificationService,
    public router: Router) {
    this.authService.userInfo.subscribe((res) => {
      this.userInfo = res;
      if (this.userInfo) {
        this.userInfo.roles.forEach(role => {
          if (role.id === Roles.Admin) {
            this.isAdmin = true;
            this.authService.isAdmin.next(true);
          } else {
            this.isAdmin = false;
            this.authService.isAdmin.next(false);
          }
          if (role.id === Roles.Leader) {
            this.isLeader = true;
            this.authService.isLeader.next(true);
          } else {
            this.isLeader = false;
            this.authService.isLeader.next(false);
          }
          if (role.id === Roles.Counselor) {
            this.isCounclingMemeber = true;
            this.authService.isCouncelor.next(true);
          } else {
            this.isCounclingMemeber = false;
            this.authService.isCouncelor.next(false);
          }
          if (role.id === Roles.Manager) {
            this.isManager = true;
            this.authService.isManager.next(true);
          } else {
            this.isManager = false;
            this.authService.isManager.next(false);
          }
          if (role.id === Roles.Member) {
            this.isMember = true;
          } else {
            this.isMember = false;
          }
          console.log(authService.isAdmin.value);

        });
      }

    });
    this.pushNotificationReg();
  }
  logout() {
    this.authService.logout().then(() => {
      this.router.navigateByUrl('/login', { replaceUrl: true });
    });
  }
  login() {
    this.router.navigateByUrl('/login', { replaceUrl: true });
  }
  async pushNotificationReg() {
    await PushNotifications.requestPermissions();
    await PushNotifications.register();
    PushNotifications.addListener('registration', (token: Token) => {
      console.log('Push registration success, token: ' + token.value);
    });
    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        console.log('Push received: ' + JSON.stringify(notification));
        this.notificationService.newnotification.next(true);
      },
    );
    let topic;
    if (Capacitor.getPlatform() === 'ios') {
      topic = 'apple';
    } else if (Capacitor.getPlatform() === 'android') {
      topic = 'google';
    }

    // set up Firebase Cloud Messaging topics
    FCM.subscribeTo({ topic })
      .then((res) => {
        console.log(`subscribed to topic “${topic}”`)
      })
      .catch((err) => {
        console.log(err);

      });

    // now you can subscribe to a specific topic
    FCM.subscribeTo({ topic: "prayer" })
      .then((r) => console.log(`subscribed to topic`))
      .catch((err) => console.log(err));

    // Unsubscribe from a specific topic
    // FCM.unsubscribeFrom({ topic: "test" })
    //   .then(() => alert(`unsubscribed from topic`))
    //   .catch((err) => console.log(err));

    // Get FCM token instead the APN one returned by Capacitor
    FCM.getToken()
      .then((r) => console.log(`Token ${r.token}`))
      .catch((err) => console.log(err));

    // // Remove FCM instance
    // FCM.deleteInstance()
    //   .then(() => alert(`Token deleted`))
    //   .catch((err) => console.log(err));

    // Enable the auto initialization of the library
    FCM.setAutoInit({ enabled: true }).then(() => console.log(`Auto init enabled`));

    // Check the auto initialization status
    FCM.isAutoInitEnabled().then((r) => {
      console.log("Auto init is " + (r.enabled ? "enabled" : "disabled"));
    });
  }
}