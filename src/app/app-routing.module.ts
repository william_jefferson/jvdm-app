import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { AutoLoginGuard } from './guards/auto-login.guard';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'task',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'aboutus',
    loadChildren: () => import('./pages/aboutus/aboutus.module').then( m => m.AboutusPageModule)
  },
  {
    path: 'contactus',
    loadChildren: () => import('./pages/contactus/contactus.module').then( m => m.ContactusPageModule)
  },
  {
    path: 'faqs',
    loadChildren: () => import('./pages/faqs/faqs.module').then( m => m.FaqsPageModule),
    canLoad: [AuthGuard],
  },
  {
    path: 'brshibuealayil',
    loadChildren: () => import('./pages/brshibuealayil/brshibuealayil.module').then( m => m.BrshibuealayilPageModule)
  },
  {
    path: 'testimonials',
    loadChildren: () => import('./pages/testimonials/testimonials.module').then( m => m.TestimonialsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule),
    canLoad: [AutoLoginGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule),
    canLoad: [AutoLoginGuard]
  },
  {
    path: 'zoom-prayer-iu',
    loadChildren: () => import('./admin/zoom-prayer-iu/zoom-prayer-iu.module').then( m => m.ZoomPrayerIuPageModule)
  },
  {
    path: 'zoom-prayer-list',
    loadChildren: () => import('./admin/zoom-prayer-list/zoom-prayer-list.module').then( m => m.ZoomPrayerListPageModule)
  },
  {
    path: 'camp-meeting-iu',
    loadChildren: () => import('./admin/camp-meeting-iu/camp-meeting-iu.module').then( m => m.CampMeetingIuPageModule)
  },
  {
    path: 'camp-meeting-list',
    loadChildren: () => import('./admin/camp-meeting-list/camp-meeting-list.module').then( m => m.CampMeetingListPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./admin/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'counsling-members',
    loadChildren: () => import('./admin/counsling-members/counsling-members.module').then( m => m.CounslingMembersPageModule)
  },
  {
    path: 'leaders',
    loadChildren: () => import('./admin/leaders/leaders.module').then( m => m.LeadersPageModule)
  },
  {
    path: 'prayer-groups',
    loadChildren: () => import('./admin/prayer-groups/prayer-groups.module').then( m => m.PrayerGroupsPageModule)
  },
  {
    path: 'coordinators',
    loadChildren: () => import('./admin/coordinators/coordinators.module').then( m => m.CoordinatorsPageModule)
  },
  {
    path: 'schedules',
    loadChildren: () => import('./admin/schedules/schedules.module').then( m => m.SchedulesPageModule)
  },
  {
    path: 'task',
    loadChildren: () => import('./pages/task/task.module').then( m => m.TaskPageModule)
  },
  {
    path: 'create-user',
    loadChildren: () => import('./admin/create-user/create-user.module').then( m => m.CreateUserPageModule)
  },
  {
    path: 'create-prayer-group',
    loadChildren: () => import('./admin/create-prayer-group/create-prayer-group.module').then( m => m.CreatePrayerGroupPageModule)
  },
  {
    path: 'locations',
    loadChildren: () => import('./admin/locations/locations.module').then( m => m.LocationsPageModule)
  },
  {
    path: 'create-schedules',
    loadChildren: () => import('./admin/create-schedules/create-schedules.module').then( m => m.CreateSchedulesPageModule)
  },
  {
    path: 'my-profile',
    loadChildren: () => import('./pages/my-profile/my-profile.module').then( m => m.MyProfilePageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./pages/notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'service-details/:servicename',
    loadChildren: () => import('./pages/service-details/service-details.module').then( m => m.ServiceDetailsPageModule)
  },
  {
    path: 'camp-registration',
    loadChildren: () => import('./admin/camp-registration/camp-registration.module').then( m => m.CampRegistrationPageModule)
  },
  {
    path: 'camp-user-update',
    loadChildren: () => import('./admin/camp-user-update/camp-user-update.module').then( m => m.CampUserUpdatePageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
