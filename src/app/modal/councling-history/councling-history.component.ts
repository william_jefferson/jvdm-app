import { Component, Input, OnInit } from '@angular/core';
import { LoadingController, AlertController, ModalController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { CounsellingService } from 'src/app/services/counselling.services';
import { UsersService } from 'src/app/services/users.services';

@Component({
  selector: 'app-councling-history',
  templateUrl: './councling-history.component.html',
  styleUrls: ['./councling-history.component.scss'],
})
export class CounclingHistoryComponent implements OnInit {
  @Input() userId: string;
  @Input() userType: string;
  
  bookings: any[] = [];
  queryparams = '';
  constructor(
    public modalController: ModalController,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private councsellingService: CounsellingService,) { }

  ngOnInit() {
    if(this.userType === 'user'){
      this.getCounselling("userId=" + this.userId);
    }else {
      this.getCounselling("counselorId=" + this.userId);
    }
  }
  dismiss() {
    this.modalController.dismiss({
      'dismissed': true
    });
  }
  async getCounselling(requestParams) {
    const loading = await this.loadingController.create();
    await loading.present();

    this.councsellingService.getCounselling(requestParams).subscribe(
      async (res) => {
        console.log(res);
        if (res.code === 200) {
          this.bookings = res.data;
        }
        await loading.dismiss();
      },
      async (res) => {
        console.log(res);
        if (res.error.code === 400) {
        }
        await loading.dismiss();
      }
    );
  }

}
