import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'in.jesusvision.app',
  appName: 'JESUS VISION',
  webDir: 'www',
  bundledWebRuntime: false,
  plugins: {
    "PushNotifications": {
      "presentationOptions": ["badge", "sound", "alert"],
    },
    "SplashScreen": {
      "launchShowDuration": 5000,
      "launchAutoHide": true,
      "androidScaleType": "CENTER_CROP",

    }
  }

};

export default config;
